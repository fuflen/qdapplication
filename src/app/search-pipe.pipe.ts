import { Pipe, PipeTransform } from '@angular/core';
import {Status} from "./models/Group";

@Pipe({
  name: 'searchPipe'
})
export class SearchPipePipe implements PipeTransform {

  transform(data: any, search?: any): any {
    if (search === undefined) {
      return data;
    } else {
      let searchText = search.toLowerCase()
      return data.filter(it =>{
        let statusNumber: number = -1;

        for (let i = 0; i < Object.keys(Status).length / 2; i++)
        {
          if (Status[i].toString().toLowerCase().indexOf(searchText) != -1) {
            statusNumber = i;
          }
        }

        return (JSON.stringify(it.name).toLowerCase().indexOf(searchText) != -1)
        || (JSON.stringify(it.accounts).indexOf(searchText) != -1)
        || (it.status == statusNumber)

      })
    }}
}



