import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent{

  @Output() searchEvent: EventEmitter<string> = new EventEmitter<string>()
  searchString: string;

  constructor() { }

  search() {
    console.log("search string: ", this.searchString)

    this.searchEvent.emit(this.searchString)
  }
}
