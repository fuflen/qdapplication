import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupsTableViewComponent } from './groups-table-view.component';

describe('GroupsTableViewComponent', () => {
  let component: GroupsTableViewComponent;
  let fixture: ComponentFixture<GroupsTableViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupsTableViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
