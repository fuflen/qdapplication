import { Component, OnInit } from '@angular/core';
import {Service} from "../service/Service";
import {Group} from "../models/Group";

@Component({
  selector: 'app-groups-table-view',
  templateUrl: './groups-table-view.component.html',
  styleUrls: ['./groups-table-view.component.scss']
})
export class GroupsTableViewComponent{

  public data$ = this.service.getData();

  filter:string;

  constructor(private service: Service) {
  }

  openModal() {
    let modal = document.getElementById("addGroupModal");
    modal.style.display = "block";
  }

  addGroup($event: Group) {
    console.log($event)
    this.service.addData($event)
  }
}
