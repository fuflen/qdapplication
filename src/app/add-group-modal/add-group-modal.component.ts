import {AfterViewInit, Component, EventEmitter, Output} from '@angular/core';
import {Group, Status} from "../models/Group";
import {Service} from "../service/Service";
import {FormBuilder, SelectControlValueAccessor} from "@angular/forms";

// noinspection JSPotentiallyInvalidUsageOfClassThis
@Component({
  selector: 'app-add-group-modal',
  templateUrl: './add-group-modal.component.html',
  styleUrls: ['./add-group-modal.component.scss']
})
export class AddGroupModalComponent{

  group: Group = new Group()
  status: string = ""
  statusEnumKeys = []
  statuses = Status
  @Output() groupEvent: EventEmitter<Group> = new EventEmitter<Group>()

  constructor() {
    this.statusEnumKeys = Object.keys(this.statuses).filter(f => isNaN(Number(f)));
    console.log(this.statusEnumKeys);
  }

  close() {
    document.getElementById("addGroupModal").style.display = 'none'
  }

  submit() {
    this.group.status = Status[this.status]
    this.groupEvent.emit(this.group)
    this.group = new Group()
    this.status = ""
    this.close()
  }
}
