﻿import {Injectable} from "@angular/core";
import {Group} from "../models/Group";
import { Observable, of} from "rxjs";
import {DATA} from "./mockData";

@Injectable({
  providedIn: 'root'
})
export class Service{

  constructor() {}

  getData() : Observable<Group[]>{
    return of(DATA)
  }

  addData(data: Group){
    DATA.push(data)
  }
}

