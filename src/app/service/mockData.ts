﻿import {Group, Status} from "../models/Group";

export const DATA: Group[] = [
  { id: 'dqefafasxc23e41234', name: 'Quantity Digital', status: Status.ACTIVE, accounts: 34 },
  { id: 'dqr32rqwfdqawsdfqw', name: 'Some Company Norway AS', status: Status.ACTIVE, accounts: 78 },
  { id: 'dqadf32r42qwrfqwef', name: 'German Export Corp', status: Status.SUSPENDED, accounts: 10  },
  { id: 'dqaewf32rt3rfwedadfwe', name: 'Another company AB', status: Status.ACTIVE, accounts: 44  },
  { id: 'fqwertf32twefr', name: 'Yet another company B.V.', status: Status.ACTIVE, accounts: 89 },
  { id: 'F2342TW3ASFF3', name: 'Another company AS', status: Status.ACTIVE, accounts: 34  },
  { id: 'df23trw<<vf34f23', name: 'Some company norway AS', status: Status.ACTIVE, accounts: 78  },
  { id: 'd23T5W<EERF<234T', name: 'Production group A/S', status: Status.SUSPENDED, accounts: 10 },
  { id: 'er2tw4twef234', name: 'Another company AB', status: Status.ACTIVE, accounts: 10 },
  { id: 'd23d32R2q3rt342', name: 'Yet another company B.V.', status: Status.ACTIVE, accounts: 89 }
];
