﻿import { v4 as uuidv4 } from 'uuid';

export class Group {

  constructor() {
    this.id = uuidv4();
  }

  id: string;
  name: string;
  status: Status;
  accounts: number;
}

export enum Status{
  ACTIVE,
  SUSPENDED
}
