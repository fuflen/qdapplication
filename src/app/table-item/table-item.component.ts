import {Component, Input, OnInit} from '@angular/core';
import {Group, Status} from "../models/Group";

@Component({
  selector: 'app-table-item',
  templateUrl: './table-item.component.html',
  styleUrls: ['./table-item.component.scss']
})
export class TableItemComponent implements OnInit {


  @Input() group: Group
  public color: string;
  public text: string;

  constructor() {
  }

  ngOnInit(): void {
   this.getColorFromStatus(this.group.status)
  }

  private getColorFromStatus(status: Status) {
    switch (status){
      case Status.ACTIVE:
        this.color = '#56B267'
        this.text = 'Active'
        break
      case Status.SUSPENDED:
        this.color = '#D69D23'
        this.text = 'Suspended'
        break
    }
  }
}
