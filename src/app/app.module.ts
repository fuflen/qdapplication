import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GroupsTableViewComponent } from './groups-table-view/groups-table-view.component';
import { TableItemComponent } from './table-item/table-item.component';
import {FormsModule} from "@angular/forms";
import { SearchComponent } from './search/search.component';
import { AddGroupModalComponent } from './add-group-modal/add-group-modal.component';
import { SearchPipePipe } from './search-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    GroupsTableViewComponent,
    TableItemComponent,
    SearchComponent,
    AddGroupModalComponent,
    SearchPipePipe
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
