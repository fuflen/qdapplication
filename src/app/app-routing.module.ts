import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GroupsTableViewComponent} from "./groups-table-view/groups-table-view.component";
import {AddGroupModalComponent} from "./add-group-modal/add-group-modal.component";

const routes: Routes = [
  {path:'', redirectTo: 'grouptableview', pathMatch:'full'},
  {path: 'grouptableview', component: GroupsTableViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
